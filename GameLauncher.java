import java.util.Scanner;

public class GameLauncher {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Hi! Please choose a game. Enter 1 to play hangman. Enter 2 to play wordle");
		int gameChoice = reader.nextInt();
		if (gameChoice == 1) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Choose a word to start hangman!");
			String word = scanner.nextLine();
			Hangman.runGame(word);
		}
		if (gameChoice == 2) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("Guess the five-letter word");
			String targetWord = Wordle.generateWord();
			Wordle.runGame(targetWord);
		}
	}
}