import java.util.Scanner;
import java.util.Random;

public class Wordle {

    public static String generateWord() {
        String[] word = new String[]{"slump", "foyer", "whack", "found", "aloft", "humor",
                "crank", "shard", "epoxy", "wince", "perky", "worst", "gecko", "mount", "query", "favor",
                "wagyu", "proxy", "taken", "ultra"};

        Random random = new Random();
        String randomWord = word[random.nextInt(word.length)];
        return randomWord;
    }

    public static boolean letterInWord(String word, char letter) {
        for (int i = 0; i < word.length(); i++) {
            if (letter == word.charAt(i)) {
                return true;
            }
        }
        return false;
    }

    public static boolean letterInSlot(String word, char letter, int position) {
        if (letter == word.charAt(position)) {
            return true;
        }
        return false;
    }

    public static String[] guessWord(String answer, String guess) {
        String[] colour = new String[]{"white", "white", "white", "white", "white"};

        for (int i = 0; i < answer.length(); i++) {
            if (letterInSlot(answer, guess.charAt(i), i)) {
                colour[i] = "green";
            } else if (letterInWord(answer, guess.charAt(i))) {
                colour[i] = "yellow";
            }
        }
        return colour;
    }

    public static void presentResults(String word, String[] colours) {
        for (int i = 0; i < word.length(); i++) {
            String color = colours[i];
            if ("green".equals(color)) {
                System.out.print("\u001B[32m" + word.charAt(i) + "\u001B[0m "); // Green
            } else if ("yellow".equals(color)) {
                System.out.print("\u001B[33m" + word.charAt(i) + "\u001B[0m "); // Yellow
            } else {
                System.out.print(word.charAt(i) + " ");
            }
        }
        System.out.println();
    }

    public static void runGame(String targetWord) {
        for (int attempt = 1; attempt <= 6; attempt++) {
            String guess = readGuess();
            String[] colors = guessWord(targetWord, guess);
            presentResults(guess, colors);

            boolean win = true;
            for (int i = 0; i < colors.length; i++) {
                if (!"green".equals(colors[i])) {
                    win = false;
                    break;
                }
            }
            if (win) {
                System.out.println("Congratulations! You win!");
                return;
            } else {
                System.out.println("Try again. Attempts left: " + (6 - attempt));
            }
        }
        System.out.println("Sorry, you've run out of attempts. The word was: " + targetWord);
    }

    public static String readGuess() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a 5-letter word guess:");
        String guess = scanner.nextLine();

        while (guess.length() != 5) {
            System.out.println("Invalid guess! Please enter a 5-letter word.");
            guess = scanner.nextLine();
        }

        return guess.toLowerCase();
    }
}
